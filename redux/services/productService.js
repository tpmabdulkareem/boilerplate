import { BaseService, HTTP_METHOD } from "./BaseService";
import { APIUrlConstants } from "../../util/APIConstants";

export class ProductServices {
  static API = APIUrlConstants.API_URLS;
  static service = new BaseService();

  // Get ProductList
  static async getAllProducts(pageNo) {
    let params = { pageNo };
    return this.service.execute(this.API.getProducts, HTTP_METHOD.GET, {
      params,
      isAuthorizedAPI: true,
    });
  }
  // Get Products By Category
  static async getProductsByCategory(categoryId) {
    let urlDirectParams = `/${categoryId}`;
    return this.service.execute(this.API.getProducts, HTTP_METHOD.GET, {
      urlDirectParams,
      isAuthorizedAPI: true,
    });
  }

  // Add ProductList
  static async addProduct(body) {
    return this.service.execute(
      this.API.createProduct,
      HTTP_METHOD.MULTIPART_POST,
      {
        body,
        isAuthorizedAPI: true,
      }
    );
  }

  // Search Products
  static async searchProducts(search) {
    let params = { search };
    return this.service.execute(this.API.getSearch, HTTP_METHOD.GET, {
      params,
      isAuthorizedAPI: false,
    });
  }

  // Delete ProductList
  static async deleteProduct(productId) {
    let urlDirectParams = `/${productId}`;
    return this.service.execute(this.API.deleteProduct, HTTP_METHOD.DELETE, {
      urlDirectParams,
      isAuthorizedAPI: true,
    });
  }

  // Add to Wishlist
  static async addWishlist(addItem) {
    let params = { addItem };
    return this.service.execute(this.API.wishlist, HTTP_METHOD.PUT, {
      params,
      isAuthorizedAPI: true,
    });
  }

  // Remove Wishlist
  static async removeWishlist(removeItem) {
    let params = { removeItem };
    return this.service.execute(this.API.wishlist, HTTP_METHOD.PUT, {
      params,
      isAuthorizedAPI: true,
    });
  }

  // Get Wishlist
  static async getWishlist() {
    return this.service.execute(this.API.getWishlist, HTTP_METHOD.GET, {
      isAuthorizedAPI: true,
    });
  }

  // Update ProductList
  static async updateProduct(productId, body) {
    let urlDirectParams = `/${productId}`;
    return this.service.execute(
      this.API.updateProduct,
      HTTP_METHOD.MULTIPART_PUT,
      {
        urlDirectParams,
        body,
        isAuthorizedAPI: true,
      }
    );
  }

  // Get Guest ProductList
  static async getAllGuestProducts(pageNo, lat, long) {
    let params = { pageNo, lat, long };
    return this.service.execute(this.API.getGuestProducts, HTTP_METHOD.GET, {
      params,
      isAuthorizedAPI: false,
    });
  }
  // Get Guest Products By Category
  static async getGuestProductsByCategory(categoryId, body) {
    let urlDirectParams = `/${categoryId}`;
    let params = { pageNo, lat, long };
    return this.service.execute(this.API.getGuestProducts, HTTP_METHOD.GET, {
      urlDirectParams,
      params,
      isAuthorizedAPI: false,
    });
  }
}
