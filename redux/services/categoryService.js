import { BaseService, HTTP_METHOD } from './BaseService';
import { APIUrlConstants } from '../../util/APIConstants';

export class CategoryServices {
  static API = APIUrlConstants.API_URLS;
  static service = new BaseService();

  // Get Category LIst
  static async getCategoryList() {
    return this.service.execute(this.API.getCategories, HTTP_METHOD.GET, {
      isAuthorizedAPI: true,
    });
  }

}
