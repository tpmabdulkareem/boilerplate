/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from "react";
import Navigation from "./navigation";
import { Provider } from "react-redux";
import { configureStore } from "./redux/store/store";;

const initialState = {};
const ReduxStore = configureStore(initialState);
const App = () => {
  // console.disableYellowBox = true;
  YellowBox.ignoreWarnings([""]);

  return (
    <Provider store={ReduxStore}>
      <Navigation />
    </Provider>
  )
};

export default App;
