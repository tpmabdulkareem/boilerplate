import { BaseService, HTTP_METHOD } from "./BaseService";
import { APIUrlConstants } from "../../util/APIConstants";

export class AuthServices {
  static API = APIUrlConstants.API_URLS;
  static service = new BaseService();

  // Login
  static async doLogin(body) {
    return this.service
      .execute(this.API.signIn, HTTP_METHOD.POST, {
        body,
        isAuthorizedAPI: false,
      })
      .then(async (response) => {
        return response;
      });
  }

  // Register
  static async doRegister(body) {
    return this.service.execute(this.API.signUp, HTTP_METHOD.POST, {
      body,
      isAuthorizedAPI: false,
    });
  }

  // Change Password
  static async doChangePassword(currentPassword, newPassword) {
    let params = { currentPassword, newPassword };
    return this.service.execute(this.API.changePassword, HTTP_METHOD.POST, {
      params,
      isAuthorizedAPI: true,
    });
  }

  // Get Profile
  static async doGetProfile() {
    return this.service.execute(this.API.user, HTTP_METHOD.GET, {
      isAuthorizedAPI: true,
    });
  }

  // Update Profile
  static async doUpdateProfile(body) {
    return this.service.execute(this.API.updateProfile, HTTP_METHOD.PUT, {
      body,
      isAuthorizedAPI: true,
    });
  }

  // Forgot Password
  static async forgotPassword(email) {
    let params = { email };
    return this.service.execute(this.API.forgotEmail, HTTP_METHOD.PUT, {
      params,
      isAuthorizedAPI: false,
    });
  }

  // OTP Verification
  static async verifyOTP(email, otp) {
    let params = { email, otp };
    return this.service.execute(this.API.verifyOtp, HTTP_METHOD.PUT, {
      params,
      isAuthorizedAPI: false,
    });
  }

  // Reset Password
  static async resetPassword(email, otp, password) {
    let params = { email, otp, password };
    return this.service.execute(this.API.resetPassword, HTTP_METHOD.PUT, {
      params,
      isAuthorizedAPI: false,
    });
  }

  // Get Notification
  static async getNotification() {
    return this.service.execute(this.API.getNotification, HTTP_METHOD.GET, {
      isAuthorizedAPI: true,
    });
  }

  // Get Promotion
  static async getPromotion() {
    return this.service.execute(this.API.promo, HTTP_METHOD.GET, {
      isAuthorizedAPI: true,
    });
  }

  // Email Verification
  static async doEmailVerification(email, otp) {
    let params = { email, otp };
    return this.service.execute(this.API.emailVerification, HTTP_METHOD.PUT, {
      params,
      isAuthorizedAPI: true,
    });
  }

  // Re Email Verification
  static async ReEmailVerification(email) {
    let params = { email };
    return this.service.execute(this.API.reRegistrationOtp, HTTP_METHOD.PUT, {
      params,
      isAuthorizedAPI: true,
    });
  }
}
