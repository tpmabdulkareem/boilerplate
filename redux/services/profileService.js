import { BaseService, HTTP_METHOD } from './BaseService';
import { APIUrlConstants } from '../../util/APIConstants';

export class ProfileServices {
  static API = APIUrlConstants.API_URLS;
  static service = new BaseService();


  // Get Profile Products
  static async getProfileProducts() {
    return this.service.execute(this.API.userProducts, HTTP_METHOD.GET, {
      isAuthorizedAPI: true,
    });
  }
}
