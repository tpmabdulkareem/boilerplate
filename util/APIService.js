import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import { APIUrlConstants } from './APIConstants';

export class APIService {
  /**
   * NOTES
   * @param urlConstant String used to refer the endpoint value
   * @param body Object that takes the request body as json/formdata
   * @param params Object that contains the parameters to pass through URL
   * @param urlDirectParams String to append after url
   * @param headers Object that contains the headers of request
   * @param isAuthorizedAPI Boolean that indicate if request should pass authorization token or not
   * @returns Object {success: true|false, status, statusText, data}
   */

  // ------------------------------------- GET ------------------------------------- //
  static async doGet(
    urlConstant,
    params = {},
    urlDirectParams = '',
    headers = {},
    isAuthorizedAPI = true,
    fullUrl,
  ) {
    let url = APIUrlConstants.getApiUrl(urlConstant) + urlDirectParams;
    if (fullUrl) {
      url = fullUrl;
    }

    if (isAuthorizedAPI)
      headers['Authorization'] = `${await AsyncStorage.getItem(
        'authToken',
      )}`;
    try {
      console.log('headers', headers);
      let response = await axios.get(url, { params, headers });
      const { status, statusText, data } = response;
      return { success: true, status, statusText, data };
    } catch (error) {
      if (error && error.response) {
        const { status, statusText, data } = error.response;
        return { success: false, status, statusText, data };
      } else {
        return {
          success: false,
          status: 500,
          statusText: 'Something went wrong',
          data: null,
        };
      }
    }
  }

  static async Get({
    fullUrl,
    params = {},
    urlDirectParams = '',
    headers = {},
    isAuthorizedAPI = true,
  }) {
    let url = APIUrlConstants.getApiFullUrl(fullUrl, urlDirectParams);

    console.log('Get url', url);
    return APIService.doGet(
      '',
      params,
      urlDirectParams,
      headers,
      isAuthorizedAPI,
      url,
    );
  }

  // ------------------------------------- POST ------------------------------------- //
  static async doPost(
    urlConstant,
    body,
    params = {},
    urlDirectParams = '',
    headers = {},
    isAuthorizedAPI = true,
    fullUrl,
  ) {
    let url = APIUrlConstants.getApiUrl(urlConstant) + urlDirectParams;
    if (fullUrl) {
      url = fullUrl;
    }
    console.log('Post url', url);
    if (isAuthorizedAPI)
      headers['Authorization'] = ` ${await AsyncStorage.getItem(
        'authToken',
      )}`;
    try {
      console.log('headers', headers);
      let response = await axios.post(url, body, {
        params: params,
        headers: headers,
      });
      const { status, statusText, data } = response;
      return { success: true, status, statusText, data };
    } catch (error) {
      console.log('Post error', error);
      if (error && error.response) {
        const { status, statusText, data } = error.response;

        return { success: false, status, statusText, data };
      } else {
        return {
          success: false,
          status: 500,
          statusText: 'Something went wrong',
          data: null,
        };
      }
    }
  }

  static async Post({
    fullUrl,
    body = {},
    params = {},
    urlDirectParams = '',
    headers = {},
    isAuthorizedAPI = true,
  }) {
    let url = APIUrlConstants.getApiFullUrl(fullUrl, urlDirectParams);
    return APIService.doPost(
      '',
      body,
      params,
      urlDirectParams,
      headers,
      isAuthorizedAPI,
      url,
    );
  }

  // ------------------------------------- PUT ------------------------------------- //
  static async doPut(
    urlConstant,
    body,
    params = {},
    urlDirectParams = '',
    headers = {},
    isAuthorizedAPI = true,
    fullUrl,
  ) {
    let url = APIUrlConstants.getApiUrl(urlConstant) + urlDirectParams;
    if (fullUrl) {
      url = fullUrl;
    }
    if (isAuthorizedAPI)
      headers['Authorization'] = `${await AsyncStorage.getItem(
        'authToken',
      )}`;
    try {
      console.log('headers', headers);
      let response = await axios.put(url, body, { params, headers });

      const { status, statusText, data } = response;
      return { success: true, status, statusText, data };
    } catch (error) {
      if (error && error.response) {
        const { status, statusText, data } = error.response;

        return { success: false, status, statusText, data };
      } else {
        return {
          success: false,
          status: 500,
          statusText: 'Something went wrong',
          data: null,
        };
      }
    }
  }

  static async Put({
    fullUrl,
    body = {},
    params = {},
    urlDirectParams = '',
    headers = {},
    isAuthorizedAPI = true,
  }) {
    let url = APIUrlConstants.getApiFullUrl(fullUrl, urlDirectParams);
    return APIService.doPut(
      '',
      body,
      params,
      urlDirectParams,
      headers,
      isAuthorizedAPI,
      url,
    );
  }

  // ------------------------------------- DELETE ------------------------------------- //
  static async doDelete(
    urlConstant,
    body,
    params = {},
    urlDirectParams = '',
    headers = {},
    isAuthorizedAPI = true,
    fullUrl,
  ) {
    let url = APIUrlConstants.getApiUrl(urlConstant) + urlDirectParams;
    if (fullUrl) {
      url = fullUrl;
    }
    if (isAuthorizedAPI)
      headers['Authorization'] = ` ${await AsyncStorage.getItem(
        'authToken',
      )}`;
    try {
      console.log('headers', headers);
      let response = await axios.delete(url, { params, headers, data: body });

      const { status, statusText, data } = response;
      return { success: true, status, statusText, data };
    } catch (error) {
      if (error && error.response) {
        const { status, statusText, data } = error.response;

        return { success: false, status, statusText, data };
      } else {
        return {
          success: false,
          status: 500,
          statusText: 'Something went wrong',
          data: null,
        };
      }
    }
  }

  static async Delete({
    fullUrl,
    body = {},
    params = {},
    urlDirectParams = '',
    headers = {},
    isAuthorizedAPI = true,
  }) {
    let url = APIUrlConstants.getApiFullUrl(fullUrl, urlDirectParams);
    return APIService.doDelete(
      '',
      body,
      params,
      urlDirectParams,
      headers,
      isAuthorizedAPI,
      url,
    );
  }

  // ------------------------------------- MULTIPLE ------------------------------------- //
  /**
   * MULTIPLE CONCURRENT REQUESTS
   * @param requestsArray Array of requests to call concurrently
   */
  static doMultipleConcurrentRequests(requestsArray) {
    axios.all(requestsArray);
  }

  // ------------------------------------- POST [FORMDATA] ------------------------------------- //
  static async doPostMultipart(
    urlConstant,
    body,
    params = {},
    urlDirectParams = '',
    headers = {},
    isAuthorizedAPI = true,
    fullUrl,
  ) {
    let url = APIUrlConstants.getApiUrl(urlConstant) + urlDirectParams;
    if (fullUrl) {
      url = fullUrl;
    }
    if (isAuthorizedAPI)
      headers['Authorization'] = ` ${await AsyncStorage.getItem(
        'authToken',
      )}`;
    try {
      console.log('headers', headers, fullUrl);
      let response = await axios({
        method: 'post',
        url,
        data: body,
        params,
        headers: {
          ...headers,
          'Content-Types': 'multipart/form-data'
        },
      });

      const { status, statusText, data } = response;
      return { success: true, status, statusText, data };
    } catch (error) {
      if (error && error.response) {
        const { status, statusText, data } = error.response;

        return { success: false, status, statusText, data };
      } else {
        return {
          success: false,
          status: 500,
          statusText: 'Something went wrong',
          data: null,
        };
      }
    }
  }

  static async PostMultipart({
    fullUrl,
    body = {},
    params = {},
    urlDirectParams = '',
    headers = {},
    isAuthorizedAPI = true,
  }) {
    let url = APIUrlConstants.getApiFullUrl(fullUrl, urlDirectParams);
    return APIService.doPostMultipart(
      '',
      body,
      params,
      urlDirectParams,
      headers,
      isAuthorizedAPI,
      url,
    );
  }
  // ------------------------------------- PUT [FORMDATA] ------------------------------------- //
  static async doPutMultipart(
    urlConstant,
    body,
    params = {},
    urlDirectParams = '',
    headers = {},
    isAuthorizedAPI = true,
    fullUrl,
  ) {
    let url = APIUrlConstants.getApiUrl(urlConstant) + urlDirectParams;
    if (fullUrl) {
      url = fullUrl;
    }
    if (isAuthorizedAPI)
      headers['Authorization'] = ` ${await AsyncStorage.getItem(
        'authToken',
      )}`;
    try {
      console.log('headers', headers);
      let response = await axios({
        method: 'put',
        url,
        data: body,
        params,
        headers: { ...headers, 'Content-Types': 'multipart/form-data' },
      });

      const { status, statusText, data } = response;
      return { success: true, status, statusText, data };
    } catch (error) {
      if (error && error.response) {
        const { status, statusText, data } = error.response;

        return { success: false, status, statusText, data };
      } else {
        return {
          success: false,
          status: 500,
          statusText: 'Something went wrong',
          data: null,
        };
      }
    }
  }

  static async PutMultipart({
    fullUrl,
    body = {},
    params = {},
    urlDirectParams = '',
    headers = {},
    isAuthorizedAPI = true,
  }) {
    let url = APIUrlConstants.getApiFullUrl(fullUrl, urlDirectParams);
    return APIService.doPutMultipart(
      '',
      body,
      params,
      urlDirectParams,
      headers,
      isAuthorizedAPI,
      url,
    );
  }

  // ------------------------------------- DOWNLOAD ------------------------------------- //
  static async doDownload({
    fullUrl,
    params = {},
    urlDirectParams = '',
    headers = {},
    isAuthorizedAPI = true,
  }) {
    let url = APIUrlConstants.getApiFullUrl(fullUrl, urlDirectParams);
    if (isAuthorizedAPI)
      headers['Authorization'] = ` ${await AsyncStorage.getItem(
        'authToken',
      )}`;
    axios({
      method: 'GET',
      url,
      params,
      headers: { ...headers },
      responseType: 'blob',
    }).then(
      resp => {
        const url = window.URL.createObjectURL(new Blob([resp.data]));
        const link = document.createElement('a');
        link.href = url;
        link.download = 'Bulk_Upload_Form_Download.xlsx';
        // link.setAttribute("download", "file.pdf");
        document.body.appendChild(link); // we need to append the element to the dom -> otherwise it will not work in firefox
        link.click();
        link.remove(); //afterwards we remove the element again

        // RESPONSE
        const { status, statusText, data } = resp;
        return { success: true, status, statusText, data };
      },
      error => {
        if (error && error.response) {
          const { status, statusText, data } = error.response;

          return { success: false, status, statusText, data };
        } else {
          return {
            success: false,
            status: 500,
            statusText: 'Something went wrong',
            data: null,
          };
        }
      },
    );
  }
  // ------------------------------------- PDFDOWNLOAD ------------------------------------- //
  static async doPDFDownload({
    fullUrl,
    params = {},
    urlDirectParams = '',
    headers = {},
    isAuthorizedAPI = true,
  }) {
    let url = APIUrlConstants.getApiFullUrl(fullUrl, urlDirectParams);
    if (isAuthorizedAPI)
      headers['Authorization'] = ` ${await AsyncStorage.getItem(
        'authToken',
      )}`;
    axios({
      method: 'GET',
      url,
      params,
      headers: { ...headers },
      responseType: 'blob',
    }).then(
      resp => {
        const url = window.URL.createObjectURL(new Blob([resp.data]));
        const link = document.createElement('a');
        link.href = url;
        link.download = 'Invoice_Download.pdf';
        // link.setAttribute("download", "file.pdf");
        document.body.appendChild(link); // we need to append the element to the dom -> otherwise it will not work in firefox
        link.click();
        link.remove(); //afterwards we remove the element again

        // RESPONSE
        const { status, statusText, data } = resp;
        return { success: true, status, statusText, data };
      },
      error => {
        if (error && error.response) {
          const { status, statusText, data } = error.response;

          return { success: false, status, statusText, data };
        } else {
          return {
            success: false,
            status: 500,
            statusText: 'Something went wrong',
            data: null,
          };
        }
      },
    );
  }
  // --------------------- ERROR NOTES --------------------- //
  // NOTE: async/await is part of ECMAScript 2017 and is not supported in Internet Explorer and older browsers, so use with caution.

  // 1xx – informational
  // 2xx – successful
  // 3xx – redirection
  // 4xx – client error
  // 5xx – server error

  // 400	Bad Request
  // 401	Not Authorized
  // 403	Forbidden
  // 404	Page/ Resource Not Found
  // 405	Method Not Allowed
  // 415	Unsupported Media Type
  // 500	Internal Server Error
}
