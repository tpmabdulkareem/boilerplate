import { Dimensions } from "react-native";
const { width, height } = Dimensions.get("window");

const constants = {
  ACTIVE_OPACITY: 0.7,
  Width: width,
  Height: height,
  BaseURL: 'https:/exaple.co/'
};
export default constants;
